package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"runtime"
	"strings"
	//"strings"
)

func processDocumentGetNetworkTask(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if req.Method != http.MethodPost {
		http.Error(w, "Invalid request method. Expected POST.", http.StatusMethodNotAllowed)
		return
	}

	var data map[string]interface{}
	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		log.Println("Error decoding JSON:", err)
		http.Error(w, "Error decoding JSON.", http.StatusBadRequest)
		return
	}

	// Получаем путь к текущему файлу (main.go)
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("Error retrieving current file path.")
		return
	}

	// Получаем директорию, в которой находится main.go
	executableDir := filepath.Dir(filename)
	// Строим относительный путь к файлу credentials.json
	credentialsFile := filepath.Join(executableDir, "credentials", "credentials.json")

	fmt.Println("Credentials file path:\n", credentialsFile)

	clientInfo := &ClientInfo{credentialsFilePath: credentialsFile}

	clientInfo.getClientOAuth(credentialsFile)

	tableWorker := &TableWorker{}
	documentWorker := &DocumentWorker{}
	googleDriveWorker := &GoogleDriveWorker{}
	googleDriveWorker.clientInfo = clientInfo

	tableWorker.clientInfo = clientInfo
	tableWorker.clientDrive = googleDriveWorker

	documentWorker.clientInfo = clientInfo
	documentWorker.clientDrive = googleDriveWorker

	jsonWorker := &JsonWorker{}
	jsonWorker.tableWorker = tableWorker
	jsonWorker.documentWorker = documentWorker
	jsonWorker.googleDriveWorker = googleDriveWorker

	if len(data) == 0 {
		log.Println("Empty data array")
		http.Error(w, "Empty data array.", http.StatusBadRequest)
		return
	}

	resultMap := data

	// Если "text" есть в мапе, извлекаем его и преобразуем в мапу
	if textData, ok := resultMap["text"]; ok {
		// Создаем map для разбора данных из "text"
		var textMap map[string]interface{}

		// Разбираем JSON из "text"
		err = json.Unmarshal([]byte(textData.(string)), &textMap)
		if err != nil {
			fmt.Println("Ошибка при разборе данных из 'text':", err)
			return
		}

		// Удаляем "text" из исходной мапы
		delete(resultMap, "text")

		// Добавляем данные из "text" в исходную мапу
		for key, value := range textMap {
			resultMap[key] = value
		}
	}

	// Выводим результат
	fmt.Printf("%+v\n", resultMap)

	newUrlFileTable, newUrlFileDoc := "", ""
	done := make(chan error)

	go func() {
		// Execute the jsonWorker.scenarioTemplate method in a goroutine
		var err error
		newUrlFileTable, newUrlFileDoc, err = jsonWorker.scenarioTemplate(resultMap)
		done <- err
	}()

	// Wait for the goroutine to complete
	err = <-done
	if err != nil {
		http.Error(w, fmt.Sprintf("Error processing scenario: %v", err), http.StatusInternalServerError)
		return
	}

	responseMessage := "table:" + newUrlFileTable + " " + "doc:" + newUrlFileDoc
	response := map[string]string{"message": responseMessage}
	log.Println("Documents processed", data)
	json.NewEncoder(w).Encode(response)

}

func processDocumentTestExistTask() {

	var data map[string]interface{}
	err := json.NewDecoder(strings.NewReader(jsonData)).Decode(&data)
	if err != nil {
		log.Println("Error decoding JSON:", err)
		return
	}

	// Получаем путь к текущему файлу (main.go)
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("Error retrieving current file path.")
		return
	}

	// Получаем директорию, в которой находится main.go
	executableDir := filepath.Dir(filename)
	// Строим относительный путь к файлу credentials.json
	credentialsFile := filepath.Join(executableDir, "credentials", "credentials.json")

	fmt.Println("Credentials file path:\n", credentialsFile)

	clientInfo := &ClientInfo{credentialsFilePath: credentialsFile}

	clientInfo.getClientOAuth(credentialsFile)

	tableWorker := &TableWorker{}
	documentWorker := &DocumentWorker{}
	googleDriveWorker := &GoogleDriveWorker{
		clientInfo:      clientInfo,
		credentialsJSON: []byte{}, // Provide the correct credentialsJSON here
	}

	tableWorker.clientInfo = clientInfo
	tableWorker.clientDrive = googleDriveWorker

	documentWorker.clientInfo = clientInfo
	documentWorker.clientDrive = googleDriveWorker

	jsonWorker := &JsonWorker{}
	jsonWorker.tableWorker = tableWorker
	jsonWorker.documentWorker = documentWorker
	jsonWorker.googleDriveWorker = googleDriveWorker

	// Execute the jsonWorker.scenarioTemplate method in a goroutine
	newUrlFileTable, newUrlFileDoc, err := jsonWorker.scenarioTemplate(data)
	fmt.Printf("newUrlFileTable %s ,newUrlFileDoc %s \n", newUrlFileTable, newUrlFileDoc)

}

func main() {

	processDocumentTestExistTask()

	/*
		log.Println("Starting document processor service!")

		corsHandler := cors.Default().Handler

		mux := http.NewServeMux()

		mux.HandleFunc("/api/v1/process_document", processDocument)
		handler := corsHandler(mux)
		server := &http.Server{
			Addr:    ":3334",
			Handler: handler,
		}
		log.Fatal(server.ListenAndServe())
	*/

}
