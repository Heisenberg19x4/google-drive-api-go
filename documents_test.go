package main
 

import (
	"fmt"
	"path/filepath"
	"runtime"
    //"log"
	"testing"
)

var CLIENTINFO1* ClientInfo

func TestAuth1(t *testing.T) {
	// Получаем путь к текущему файлу (main.go)
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("Error retrieving current file path.")
		return
	}

	// Получаем директорию, в которой находится main.go
	executableDir := filepath.Dir(filename)
    // Строим относительный путь к файлу credentials.json
    credentialsFile := filepath.Join(executableDir, "credentials", "credentials.json")

    fmt.Println("Credentials file path:\n", credentialsFile)

	
	clientInfo := &ClientInfo{credentialsFilePath: credentialsFile}

	clientInfo.getClientOAuth(credentialsFile)
	CLIENTINFO1=clientInfo
}


func TestDocumentCreate(t *testing.T) {

	documentWorker := &DocumentWorker{}
	documentWorker.clientInfo = CLIENTINFO1
	documentWorker.clientDrive = &GoogleDriveWorker{CLIENTINFO1}


	documentName := "MyDocument.docx"
	folderName := "hbddschj"
	documentWorker.clientDrive.createFolder(folderName)

	documentID, err := documentWorker.CreateDocument(documentName, folderName)
	if err != nil {
		fmt.Printf("Error creating document: %v\n", err)
	} else {
		fmt.Printf("Document ID: %s\n", documentID)
	}
}
