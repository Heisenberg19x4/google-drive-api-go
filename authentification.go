package main
import (
    "context"
    "encoding/json"
    "fmt"
    "log"
    "net/http"
    "os"

    "golang.org/x/oauth2"
    "golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/docs/v1"
	"google.golang.org/api/people/v1"


)

type ClientInfo struct{

	credentialsFilePath string
	currentID string
	login string
	password string

	driveClientSheetsService *sheets.Service // api for work with table
	driveClientDocService    *docs.Service
	driveClientService *drive.Service //api foe google drive operation
	TokenFromWeb *oauth2.Token

}


func (p *ClientInfo) getClientOAuth(credentialsFile string) error {
	p.credentialsFilePath=credentialsFile
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return fmt.Errorf("reading client secret file: %w", err)
	}

	scopes := []string{
		docs.DocumentsScope,
		docs.DocumentsReadonlyScope,
		drive.DriveFileScope,
		drive.DriveAppdataScope,
		drive.DriveMetadataReadonlyScope,
		drive.DriveReadonlyScope,
		sheets.SpreadsheetsScope,
		sheets.SpreadsheetsReadonlyScope,
		"https://www.googleapis.com/auth/script.projects",
		people.UserinfoProfileScope,
	}

	config, err := google.ConfigFromJSON(b, scopes...)
	if err != nil {
		return fmt.Errorf("parsing client secret file to config: %w", err)
	}


	client := p.getClientWithToken(config)


	// For Docs
	docsService, err := docs.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Docs client: %w", err)
	}
	p.driveClientDocService = docsService

	// For Drive
	driveService, err := drive.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Drive client: %w", err)
	}
	p.driveClientService = driveService

	// For Sheets
	sheetsService, err := sheets.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Sheets client: %w", err)
	}
	p.driveClientSheetsService = sheetsService

	return nil
}

func (p *ClientInfo) getClientDocsOAuth(credentialsFile string) error {
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return fmt.Errorf("reading client secret file: %w", err)
	}

	config, err := google.ConfigFromJSON(b, docs.DocumentsScope)
	if err != nil {
		return fmt.Errorf("parsing client secret file to config: %w", err)
	}

	client := p.getClientWithToken(config)

	srv, err := docs.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Docs client: %w", err)
	}

	p.driveClientDocService = srv

	return nil
}

func (p *ClientInfo) getDriveServiceOAuth(credentialsFile string) error {
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return fmt.Errorf("reading client secret file: %w", err)
	}

	config, err := google.ConfigFromJSON(b, drive.DriveFileScope)
	if err != nil {
		return fmt.Errorf("parsing client secret file to config: %w", err)
	}

	client := p.getClientWithToken(config)

	driveService, err := drive.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Drive client: %w", err)
	}

	p.driveClientService = driveService
	return nil
}

func (p *ClientInfo) getClientSheetsOAuth(credentialsFile string) error {
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return fmt.Errorf("reading client secret file: %w", err)
	}

	config, err := google.ConfigFromJSON(b, sheets.SpreadsheetsScope)
	if err != nil {
		return fmt.Errorf("parsing client secret file to config: %w", err)
	}

	client := p.getClientWithToken(config)

	srv, err := sheets.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Sheets client: %w", err)
	}

	p.driveClientSheetsService = srv

	return nil
}


func (p *ClientInfo) getClientDocsServiceAccount(credentialsFile string) error {
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return fmt.Errorf("reading client secret file: %w", err)
	}

	config, err := google.JWTConfigFromJSON(b, docs.DocumentsScope)
	if err != nil {
		return fmt.Errorf("parsing client secret file to config: %w", err)
	}

	//client := p.getClientWithToken(config)
	client := config.Client(context.Background())

	srv, err := docs.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Docs client: %w", err)
	}

	p.driveClientDocService = srv

	return nil
}

func (p *ClientInfo) getDriveServiceServiceAccount(credentialsFile string) ( error) {
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return fmt.Errorf("reading client secret file: %w", err)
	}

	config, err := google.JWTConfigFromJSON(b, drive.DriveFileScope)
	if err != nil {
		return fmt.Errorf("parsing client secret file to config: %w", err)
	}

	client := config.Client(context.Background())

	driveService, err := drive.New(client)
	if err != nil {
		return fmt.Errorf("unable to retrieve Drive client: %w", err)
	}

	p.driveClientService=driveService
	return  nil
}


func (p* ClientInfo)getClientSheetsServiceAccount(credentialsFile string) (error) {
	b, err := os.ReadFile(credentialsFile)
	if err != nil {
		return  fmt.Errorf("reading client secret file: %w", err)
	}

	// Create a JWT config using the key file.
	config, err := google.JWTConfigFromJSON(b, sheets.SpreadsheetsScope)
	if err != nil {
		return  fmt.Errorf("parsing client secret file to config: %w", err)
	}

	// Create a client using the JWT config.
	client := config.Client(context.Background())

	srv, err := sheets.New(client)
	if err != nil {
		return  fmt.Errorf("unable to retrieve Sheets client: %w", err)
	}

	p.driveClientSheetsService=srv

	return  nil
}





func (p* ClientInfo)tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	p.TokenFromWeb=tok
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}


func (p* ClientInfo)getTokenFromWeb(config *oauth2.Config) (*oauth2.Token,error) {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser, then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.Background(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	p.TokenFromWeb=tok
	return tok, nil
}


func (p* ClientInfo)saveToken(file string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", file)
	f, err := os.Create(file)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func (p* ClientInfo)getClientWithToken(config *oauth2.Config) *http.Client {
	tokFile := "token.json"
	tok, err := p.tokenFromFile(tokFile)
	if err != nil {
		tok, err = p.getTokenFromWeb(config)
		if err != nil {
			log.Fatalf("DONT PARSE token: %v", err)
		}
		p.saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}





