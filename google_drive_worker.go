package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/people/v1"
)

type GoogleDriveWorker struct {
	credentialsJSON []byte
	clientInfo      *ClientInfo
}

type CommentInfo struct {
	Comment     *drive.Comment
	CellAddress string
}

func (p *GoogleDriveWorker) createFolder(folderName string) (string, error) {
	srv := p.clientInfo.driveClientService

	// Create a folder in the root directory
	folder := &drive.File{
		Name:     folderName,
		MimeType: "application/vnd.google-apps.folder",
	}
	file, err := srv.Files.Create(folder).Context(context.Background()).Do()
	if err != nil {
		return "", fmt.Errorf("unable to create folder: %w", err)
	}

	// Get the ID of the created folder
	folderID := file.Id
	fmt.Printf("Folder '%s' created with ID: %s\n", folderName, folderID)
	return folderID, nil
}

func (p *GoogleDriveWorker) GetFolderID(folderName string) (string, error) {

	// Получаем все папки
	folderList, err := p.clientInfo.driveClientService.Files.List().Q("mimeType='application/vnd.google-apps.folder'").Do()
	if err != nil {
		return "", fmt.Errorf("ошибка при получении списка папок: %v", err)
	}

	// Ищем среди всех папок
	for _, folder := range folderList.Files {
		fmt.Printf("folder.Name %s\n", folder.Name)
		if folder.Name == folderName {
			return folder.Id, nil
		}
	}

	// Если папка не найдена
	return "", fmt.Errorf("папка '%s' не найдена", folderName)
}

func (p *GoogleDriveWorker) MoveToFolder(fileID string, folderID string) error {
	// Get the current parents of the file
	file, err := p.clientInfo.driveClientService.Files.Get(fileID).Fields("parents").Do()
	if err != nil {
		return fmt.Errorf("ошибка при получении родительских папок: %v", err)
	}

	currentParents := strings.Join(file.Parents, ",")

	// Build the parents parameter for the update request
	newParents := strings.Replace(currentParents, folderID, "", -1)
	if newParents != "" {
		newParents += ","
	}
	newParents += folderID

	// Perform the update to move the file to the new folder
	_, err = p.clientInfo.driveClientService.Files.Update(fileID, &drive.File{
		Parents: []string{}, // Clear existing parents
	}).AddParents(newParents).RemoveParents(currentParents).Do()

	if err != nil {
		return fmt.Errorf("ошибка при перемещении таблицы в папку: %v", err)
	}

	return nil
}

// ***************************************************************************************************************************
// _____________________________Comments work_________________________________________
// ***************************************************************************************************************************

func (p *GoogleDriveWorker) getUserInfo(token *oauth2.Token) (*drive.User, error) {

	credentialsJSON, err := os.ReadFile(p.clientInfo.credentialsFilePath)
	if err != nil {
		return nil, fmt.Errorf("reading client secret file: %w", err)
	}

	fmt.Printf("drive.DriveFileScope %s\n", drive.DriveFileScope)
	fmt.Printf(" people.UserinfoProfileScope %s\n", people.UserinfoProfileScope)

	config, err := google.ConfigFromJSON([]byte(credentialsJSON), drive.DriveFileScope, people.UserinfoProfileScope)
	if err != nil {
		return nil, err
	}
	fmt.Printf("config %s\n", config)

	client := config.Client(context.Background(), token)
	fmt.Printf("client %s\n", client)

	// Retrieve user information from the Google People API
	peopleService, err := people.New(client)
	if err != nil {
		return nil, err
	}
	fmt.Printf("peopleService %s\n", peopleService)

	person, err := peopleService.People.Get("people/me").PersonFields("names,emailAddresses,photos").Do()
	if err != nil {
		return nil, err
	}
	fmt.Printf("person %s\n", person)
	// Populate the drive.User struct
	user := &drive.User{}

	if len(person.EmailAddresses) > 0 {
		user.EmailAddress = person.EmailAddresses[0].Value
	} else {
		user.EmailAddress = "modulotcetov@gmail.com"
	}

	// Проверка наличия фотографии
	if len(person.Photos) > 0 {
		user.PhotoLink = person.Photos[0].Url
	}

	return user, nil
}

// что вставляем, на какой текст
func (p *GoogleDriveWorker) createComment(fileID, content, targetText string, rowIndex, columnIndex int64) (string, error) {
	// Assume p.TokenFromWeb contains the user's access token obtained during the authentication process.
	userInfo, err := p.getUserInfo(p.clientInfo.TokenFromWeb)
	if err != nil {
		return "", err
	}

	// Вызов revisions.list для получения списка всех revisionID документа
	revisions, err := p.clientInfo.driveClientService.Revisions.List(fileID).Fields("revisions(id)").Do()
	if err != nil {
		log.Fatalf("Не удалось получить список revisionID: %v", err)
	}

	// Проход по списку revisionID
	for _, revision := range revisions.Revisions {
		fmt.Printf(" revision.Id=%s  revision=%s\n", revision.Id, revision)
	}

	anchor := map[string]interface{}{
		"r": "head",
	}

	anchorJSON, err := json.Marshal(anchor)
	if err != nil {
		log.Fatalf("Не удалось маршализовать Anchor в JSON: %v", err)
	}

	// Создаем объект CommentQuotedFileContent для указания текста, к которому привязывается комментарий
	quotedFileContent := &drive.CommentQuotedFileContent{
		MimeType: "text/plain", // Замените на соответствующий MIME-тип вашего текста
		Value:    targetText,
	}

	// Создаем объект Comment с включенным полем QuotedFileContent
	comment := &drive.Comment{
		Content:           content,
		Author:            userInfo,
		Anchor:            string(anchorJSON),
		Resolved:          false,
		QuotedFileContent: quotedFileContent,
	}

	commentInfo, err := p.clientInfo.driveClientService.Comments.Create(fileID, comment).Fields("id", "author(displayName)", "content").Do()
	if err != nil {
		log.Fatalf("Не удалось получить информацию о созданном комментарии: %v", err)
	}

	//fmt.Printf("commentInfo %s",commentInfo)
	fmt.Printf("Комментарий успешно создан: ID=%s, Автор=%s, Текст=%s\n", commentInfo.Id, commentInfo.Author.DisplayName, commentInfo.Content)

	fmt.Println("____COMMENT INFO___")
	comments, err := p.getCommentsFromDriveFile(fileID)
	if err != nil {
		log.Fatalf("Ошибка при получении списка комментариев: %v", err)
	}
	fmt.Println(comments)

	return commentInfo.Id, nil
}

func (p *GoogleDriveWorker) updateComment(fileID, commentID, newContent, newTargetText string) error {
	// Assume p.TokenFromWeb contains the user's access token obtained during the authentication process.

	updatedComment := &drive.Comment{
		Content: newContent,
		QuotedFileContent: &drive.CommentQuotedFileContent{
			MimeType: "text/plain",
			Value:    newTargetText,
		},
	}

	// Call the Comments.Update method to update the comment
	updatedComment, err := p.clientInfo.driveClientService.Comments.Update(fileID, commentID, updatedComment).Fields("content").Do()
	if err != nil {
		return fmt.Errorf("Failed to update comment: %v", err)
	}

	fmt.Println("Comment successfully updated")

	return nil
}

func (p *GoogleDriveWorker) getCommentsFromDriveFile(fileID string) ([]*drive.Comment, error) {
	commentsList, err := p.clientInfo.driveClientService.Comments.List(fileID).
		Fields("comments(id,author(displayName),content,anchor)"). // Указываем необходимые поля
		Context(context.Background()).Do()
	if err != nil {
		log.Fatalf("Unable to get getCommentsFromDriveFile info: %v", err)

		return nil, err
	}
	fmt.Println("commentsList")
	fmt.Println(commentsList)
	comments := commentsList.Comments // Получаем срез комментариев из CommentList
	fmt.Println("commentsList.Comments")
	fmt.Println(commentsList.Comments)
	// Вывод информации о каждом комментарии
	for _, comment := range comments {
		fmt.Printf("Комментарий ID=%s, Автор=%s, Текст=%s, Решен=%t\n", comment.Id, comment.Author.DisplayName, comment.Content, comment.Resolved)
		if comment.QuotedFileContent != nil {
			fmt.Printf("QuotedFileContent: MIME Type=%s, Value=%s\n", comment.QuotedFileContent.MimeType, comment.QuotedFileContent.Value)
		}
	}
	/*
		    fmt.Println("QuotedFileContent")
		    fmt.Println(comments[0].QuotedFileContent)
			commentInfoList, err := p.getCellInfoForComments(fileID, comments)
			if err != nil {
				log.Fatalf("Unable to get cell info: %v", err)
			}
		    fmt.Println("commentInfoList")
		    fmt.Println(commentInfoList)

			fmt.Println("Информация о комментариях и ячейках:")
			for _, commentInfo := range commentInfoList {
				fmt.Printf("Комментарий ID: %s, Текст: %s\n", commentInfo.Comment.Id, commentInfo.Comment.Content)
				fmt.Printf("Ячейка: %s\n", commentInfo.CellAddress)
				fmt.Printf("Анкер: %s\n", commentInfo.Comment.Anchor) // Добавляем вывод анкера
				fmt.Println("-----")
			}
	*/

	return comments, nil
}

func (p *GoogleDriveWorker) getCellInfoForComments(spreadsheetID string, comments []*drive.Comment) ([]CommentInfo, error) {
	var commentInfoList []CommentInfo

	for _, comment := range comments {
		// Разбираем anchor комментария, чтобы получить адрес ячейки
		cellAddress, err := p.parseAnchorToCellAddress(comment.Anchor)
		if err != nil {
			return nil, err
		}

		commentInfo := CommentInfo{
			Comment:     comment,
			CellAddress: cellAddress,
		}

		commentInfoList = append(commentInfoList, commentInfo)
	}

	return commentInfoList, nil
}
func (p *GoogleDriveWorker) parseAnchorToCellAddress(anchor string) (string, error) {
	fmt.Printf("Anchor: %s\n", anchor)

	var parsedAnchor map[string]interface{}
	err := json.Unmarshal([]byte(anchor), &parsedAnchor)
	if err != nil {
		return "", err
	}

	if parsedAnchor["type"] == "workbook-range" {
		// Пример анкера: {"type":"workbook-range","uid":0,"range":"1126449076"}
		rangeValue, rangeExists := parsedAnchor["range"].(string)
		if rangeExists {
			return rangeValue, nil
		}
	}

	return "", fmt.Errorf("Invalid anchor format")
}

func (p *GoogleDriveWorker) getColumnLetter(columnNumber int) string {
	dividend := columnNumber
	var columnName string

	for dividend > 0 {
		modulo := (dividend - 1) % 26
		columnName = string('A'+modulo) + columnName
		dividend = (dividend - 1) / 26
	}

	return columnName
}
