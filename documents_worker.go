package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"google.golang.org/api/docs/v1"
	"google.golang.org/api/drive/v3"

	//"google.golang.org/api/option"
	"errors"

	"regexp"
	"strings"
)

type DocumentWorker struct {
	DocumentName     string
	DocFileByteArray []byte
	clientInfo       *ClientInfo
	clientDrive      *GoogleDriveWorker
}

func (p *DocumentWorker) CopyDocumentTemplate(documentID, copyName string, parentFolderID string) (string, error) {
	// Prepare a request to create a copy
	copyRequest := &drive.File{
		Name:    copyName,
		Parents: []string{parentFolderID},
	}

	// Create the copied document
	copiedDoc, err := p.clientInfo.driveClientService.Files.Copy(documentID, copyRequest).Context(context.Background()).Do()
	if err != nil {
		return "", fmt.Errorf("unable to create copied document: %v", err)
	}

	fmt.Printf("Document template copied to '%s' with ID: %s\n", copyName, copiedDoc.Id)
	return copiedDoc.Id, nil
}

// **************************************************
// _______________ID DOCUMENT_______________
// **************************************************

func (p *DocumentWorker) GetDocumentIDByNameAndPath(documentName, folderID string) (string, error) {
	query := fmt.Sprintf("name='%s' and mimeType='application/vnd.google-apps.document' and '%s' in parents", documentName, folderID)
	fmt.Printf("GetDocumentIDByNameAndPath query %v", query)

	files, err := p.clientInfo.driveClientService.Files.List().Q(query).Do()
	if err != nil {
		log.Printf("ошибка при получении списка файлов: %v\n", err)
		return "", err
	}
	fmt.Printf("GetDocumentIDByNameAndPath files %v", files)

	// Поиск документа с заданным именем
	for _, file := range files.Files {
		if file.Name == documentName {
			return file.Id, nil
		}
	}

	return "", fmt.Errorf("документ с именем %s не найден в папке %s", documentName, folderID)

}

// **************************************************
// _______________variables and cells_______________
// **************************************************
func (p *DocumentWorker) replaceTextDocument(documentID, searchText, replacementText string) error {
	ctx := context.Background()

	// Prepare the replace request
	replaceRequest := &docs.ReplaceAllTextRequest{
		ReplaceText: replacementText,
		ContainsText: &docs.SubstringMatchCriteria{
			MatchCase: true, // Set to true if the match should be case-sensitive
			Text:      searchText,
		},
	}

	// Create the batch update request with the replace request
	batchUpdateRequest := &docs.BatchUpdateDocumentRequest{
		Requests: []*docs.Request{
			{ReplaceAllText: replaceRequest},
		},
	}

	// Execute the batch update request
	_, err := p.clientInfo.driveClientDocService.Documents.BatchUpdate(documentID, batchUpdateRequest).Context(ctx).Do()
	if err != nil {
		return fmt.Errorf("failed to replace text in document: %v", err)
	}

	fmt.Printf("Text replaced successfully in the document.\n")

	_, err = p.clientDrive.createComment(documentID, searchText, replacementText, 1, 0)
	if err != nil {
		log.Printf("Failed to create comment: %v", err)
	}

	return nil
}

// Helper function to check if a paragraph contains a specific title
func (p *DocumentWorker) containsTitle(paragraph *docs.Paragraph, title string) bool {
	for _, element := range paragraph.Elements {
		if element.TextRun != nil && element.TextRun.Content == title {
			return true
		}
	}
	return false
}

func (p *DocumentWorker) replaceImageInDocument(documentID string, newImageUrls []string) error {
	ctx := context.Background()

	// Получаем документ
	doc, err := p.clientInfo.driveClientDocService.Documents.Get(documentID).Context(ctx).Do()
	if err != nil {
		return fmt.Errorf("ошибка получения документа: %v", err)
	}

	newImageIndex := 0

	for _, element := range doc.Body.Content {
		// Проверяем, является ли элемент параграфом
		if element.Paragraph != nil {
			for _, paragraphElement := range element.Paragraph.Elements {
				// Проверяем, является ли элемент изображением
				if paragraphElement.InlineObjectElement != nil {
					inlineObjectId := paragraphElement.InlineObjectElement.InlineObjectId

					// Проверяем, есть ли еще новые изображения для замены
					if newImageIndex >= len(newImageUrls) {
						return nil // No more images to replace
					}

					// Создаем объект запроса для обновления изображения
					replaceImageRequest := &docs.Request{
						ReplaceImage: &docs.ReplaceImageRequest{
							ImageObjectId: inlineObjectId,
							Uri:           newImageUrls[newImageIndex],
						},
					}

					// Отправляем запрос на замену изображения в документе
					_, err := p.clientInfo.driveClientDocService.Documents.BatchUpdate(documentID, &docs.BatchUpdateDocumentRequest{
						Requests: []*docs.Request{replaceImageRequest},
					}).Context(ctx).Do()
					if err != nil {
						return fmt.Errorf("ошибка замены изображения: %v", err)
					}

					fmt.Printf("Изображение успешно заменено: %s\n", newImageUrls[newImageIndex])

					// Переходим к следующему новому изображению
					newImageIndex++
					fmt.Printf("newImageIndex %v\n", newImageIndex)
				}
			}
		}
	}

	return nil
}

func (p *DocumentWorker) findKeyInJSON(data map[string]interface{}, nameKey string) (string, []string, bool) {

	fmt.Println("__________findKeyInJSON__________nameKey", nameKey)
	answerValue := []string{}
	findValue := false

	for key, value := range data {
		fmt.Printf("__________findKeyInJSON  value %v , key %v\n", value, key)
		if nameKey == "Картинки" && key == "Картинки" {
			imageMap, ok := value.(map[string]interface{})
			if !ok {
				// Handle the case where the value is not a map[string]interface{}
				fmt.Println("Error: Value is not a map[string]interface{}")
				return nameKey, nil, false
			}

			for _, vImg := range imageMap {
				// Type assert the value inside the nested map to string
				if imgUrl, ok := vImg.(string); ok {
					answerValue = append(answerValue, imgUrl)
				} else {
					// Handle the case where the value inside the nested map is not a string
					fmt.Println("Error: Value inside the nested map is not a string")
					return nameKey, nil, false
				}
			}
			// Now answerValue contains the URLs
			fmt.Println("НАШЛИ КАРТИНКИ", answerValue)
			return nameKey, answerValue, true
		}
		switch v := value.(type) {
		case string:
			fmt.Printf(" case string: =%v  %v\n", key, nameKey)

			if key == nameKey {
				// Найден ключ, возвращаем его и значение
				answerValue = append(answerValue, fmt.Sprintf("%v", value))
				findValue = true
				fmt.Printf("НАШЛИ case string: =%v\n", answerValue)
				//return key, answerValue, true
			}
		case int:
			fmt.Printf(" case int: =%v  %v\n", key, nameKey)

			if key == nameKey {
				// Найден ключ, возвращаем его и значение
				answerValue = append(answerValue, fmt.Sprintf("%v", value))
				findValue = true
				fmt.Printf("НАШЛИ case int: =%v\n", answerValue)
			}
		case float64:
			fmt.Printf(" case float64: =%v  %v\n", key, nameKey)

			if key == nameKey {
				// Найден ключ, возвращаем его и значение
				answerValue = append(answerValue, fmt.Sprintf("%v", value))
				findValue = true
				fmt.Printf("НАШЛИ case float64: =%v\n", answerValue)
			}
		case []interface{}:

			fmt.Println(value)
			flagContains := false
			var firstKey string
			var lastKey string

			if strings.Contains(nameKey, ".") {
				fmt.Println(nameKey)
				partsKey := strings.Split(nameKey, ".") //[]string
				flagContains = true
				firstKey = partsKey[0]
				lastKey = strings.Join(partsKey[1:], ".")

			} else {
				fmt.Println(nameKey)
				firstKey = ""
				lastKey = nameKey

			}
			if key == firstKey || flagContains == false {
				for _, item := range v {
					fmt.Printf("__________findKeyInJSON  item %v  \n", item)

					nestedObj, ok := item.(map[string]interface{})
					if !ok {
						continue
					}
					fmt.Printf("__________findKeyInJSON  nestedObj %v  \n", nestedObj)

					for nestedKey, nestedValue := range nestedObj {
						fmt.Printf("__________findKeyInJSON  nestedKey %v , nestedValue %v\n", nestedKey, nestedValue)

						if nestedKey == lastKey {
							// Найден ключ, возвращаем его и значение
							answerValue = append(answerValue, fmt.Sprintf("%v", nestedValue))
							fmt.Printf("НАШЛИ answerValue =%v\n", answerValue)
							findValue = true
						}
					}
				}
				//return lastKey, answerValue, true
			}
		case map[string]interface{}:

			flagContains := false
			var firstKey string
			var lastKey string

			if strings.Contains(nameKey, ".") {

				partsKey := strings.Split(nameKey, ".") //[]string
				flagContains = true
				firstKey = partsKey[0]
				lastKey = strings.Join(partsKey[1:], ".")

			} else {
				firstKey = ""
				lastKey = nameKey

			}

			if key == firstKey || flagContains == false {
				for nestedKey, nestedValue := range v {
					if nestedKey == lastKey {
						// Найден ключ, возвращаем его и значение
						answerValue = append(answerValue, fmt.Sprintf("%v", nestedValue))
						findValue = true
					}
				}
				//return lastKey, answerValue, true

			}
		default:
			fmt.Println("Неизвестный тип")

		}

	}
	// Ключ не найден
	return nameKey, answerValue, findValue
}

// findTableInDocument находит таблицу в документе по ID и выводит ее содержимое.
func (p *DocumentWorker) findTableInDocument(documentID string) (*docs.Table, int64, int64, int64, error) {
	ctx := context.Background()

	// Получаем документ
	doc, err := p.clientInfo.driveClientDocService.Documents.Get(documentID).Context(ctx).Do()
	if err != nil {
		return nil, 0, 0, 0, fmt.Errorf("ошибка получения документа: %v", err)
	}
	var indexTable int64
	// Перебираем структурные элементы
	for i, element := range doc.Body.Content {
		if element.Table != nil {
			indexTable = int64(i)
			tableContent, err := p.getTableContent(documentID, element.Table.TableRows[0].TableCells[0].Content)
			if err != nil {
				return nil, 0, 0, 0, err
			}

			fmt.Printf("Содержимое таблицы (StartIndex: %d, EndIndex: %d):\n", element.StartIndex, element.EndIndex)
			fmt.Println(tableContent)

			// Вызываем метод для добавления пустой строки в конец таблицы

			err = p.addEmptyRowToEnd(documentID, indexTable, element.StartIndex, element.EndIndex)
			if err != nil {
				return nil, 0, 0, 0, err
			}

			return element.Table, indexTable, element.StartIndex, element.EndIndex, nil
		}
	}

	// Таблица не найдена
	return nil, 0, 0, 0, errors.New("таблица не найдена в документе")
}

// addEmptyRowToEnd добавляет пустую строку в конец таблицы в Google Документах.
func (p *DocumentWorker) addEmptyRowToEnd(documentID string, indexTable, startIndex, endIndex int64) error {
	ctx := context.Background()

	fmt.Printf("startIndex%v\n", startIndex)
	fmt.Printf("endIndex%v\n", endIndex)
	//lastCellIndex := startIndex
	//lastRowIndex :=startIndex
	// Получаем документ
	doc, err := p.clientInfo.driveClientDocService.Documents.Get(documentID).Context(ctx).Do()
	if err != nil {
		return fmt.Errorf("ошибка получения документа: %v", err)
	}
	// Проверяем, есть ли какой-либо контент в документе
	if len(doc.Body.Content) == 0 {
		return errors.New("документ не содержит контента")
	}

	lenRows := doc.Body.Content[indexTable].Table.Rows
	// Находим индекс конечной ячейки в последней строке таблицы
	/*
		fmt.Printf("Rows %v\n", lenRows)
		fmt.Printf("Columns %v\n", doc.Body.Content[indexTable].Table.Columns)
		fmt.Printf("SuggestedDeletionIds %v\n", doc.Body.Content[indexTable].Table.SuggestedDeletionIds)
		fmt.Printf("SuggestedInsertionIds %v\n", doc.Body.Content[indexTable].Table.SuggestedInsertionIds)

		fmt.Printf("lastRow %v\n", len(doc.Body.Content[indexTable].Table.TableRows))
		fmt.Printf("TableRows %v\n", doc.Body.Content[indexTable].Table.TableRows[0])
		fmt.Printf("TableRows StartIndex%v\n", doc.Body.Content[indexTable].Table.TableRows[0].StartIndex)
		fmt.Printf("TableRows EndIndex%v\n", doc.Body.Content[indexTable].Table.TableRows[0].EndIndex)
		fmt.Printf("TableCell %v\n", len(doc.Body.Content[indexTable].Table.TableRows[0].TableCells))
		fmt.Printf("TableCell %v\n", doc.Body.Content[indexTable].Table.TableRows[0].TableCells)

		fmt.Printf("TableRows %v\n", doc.Body.Content[indexTable].Table.TableRows[1])
		fmt.Printf("TableRows StartIndex%v\n", doc.Body.Content[indexTable].Table.TableRows[1].StartIndex)
		fmt.Printf("TableRows EndIndex%v\n", doc.Body.Content[indexTable].Table.TableRows[1].EndIndex)
		fmt.Printf("TableCell %v\n", len(doc.Body.Content[indexTable].Table.TableRows[1].TableCells))
		fmt.Printf("TableCell %v\n", len(doc.Body.Content[indexTable].Table.TableRows[2].TableCells))
		fmt.Printf("Content%v\n",doc.Body.Content[indexTable].Table.TableRows[lenRows-2].TableCells[0].Content)
		fmt.Printf("Content%v\n",doc.Body.Content[indexTable].Table.TableRows[lenRows-1].TableCells[0].Content)


		for _,contentCell:=range doc.Body.Content[indexTable].Table.TableRows[lenRows-1].TableCells[0].Content{
			fmt.Printf("contentCell %v\n",contentCell)
			fmt.Printf("startIndex%v\n",contentCell.StartIndex)
			fmt.Printf("endIndex%v\n",contentCell.EndIndex)
			lastRowIndex =contentCell.EndIndex
			lastCellIndex=contentCell.StartIndex
			fmt.Printf("lastCellIndex%v\n",lastCellIndex)
		}

	*/

	// Создаем запрос для вставки пустой строки
	insertRequest := &docs.Request{
		InsertTableRow: &docs.InsertTableRowRequest{
			TableCellLocation: &docs.TableCellLocation{
				ColumnIndex: 0,
				RowIndex:    lenRows - 1,
				TableStartLocation: &docs.Location{
					Index: startIndex,
				},
			},
			InsertBelow: true, // Вставляем строку ниже последней строки таблицы
		},
	}
	// Выполняем запрос на вставку пустой строки
	_, err = p.clientInfo.driveClientDocService.Documents.BatchUpdate(documentID, &docs.BatchUpdateDocumentRequest{
		Requests: []*docs.Request{insertRequest},
	}).Context(ctx).Do()

	if err != nil {

		return fmt.Errorf("ошибка при вставке пустой строки: %v", err)

	}
	return nil

}

// getTableContent получает содержимое таблицы в виде строкового представления.
func (p *DocumentWorker) getTableContent(documentID string, content []*docs.StructuralElement) (string, error) {
	var tableContent string

	// Перебираем структурные элементы в содержимом ячейки таблицы
	for _, element := range content {
		// Получаем текст из элемента
		text := p.getTextContent(element)
		if text != "" {
			tableContent += text
		}
	}

	return tableContent, nil
}

// getTextContent извлекает текст из структурного элемента.
func (p *DocumentWorker) getTextContent(element *docs.StructuralElement) string {
	var text string

	if element.Paragraph != nil {
		for _, run := range element.Paragraph.Elements {
			text += run.TextRun.Content
		}
	} else if element.Table != nil {
		// Рекурсивно вызываем getTableContent для обработки содержимого ячейки таблицы
		tableContent, _ := p.getTableContent("", element.Table.TableRows[0].TableCells[0].Content)
		text += tableContent
	}

	return text
}

func (p *DocumentWorker) deleteTable(documentID string, doctable *docs.Table, StartIndex, EndIndex int64) error {
	ctx := context.Background()

	// Check if the table has structural elements
	if len(doctable.TableRows) > 0 {
		// Get the first structural element containing the table .TableRows[0].TableCells[0].Content
		structuralElement := doctable.TableRows[0].TableCells[0]

		// Check if the structural element has a range
		if structuralElement != nil {
			// Delete the content range using the range of the structural element
			fmt.Printf("structuralElement.StartIndex %v\n", structuralElement.StartIndex)
			fmt.Printf("structuralElement.EndIndex %v\n", structuralElement.EndIndex)

			deleteRequest := &docs.Request{
				DeleteContentRange: &docs.DeleteContentRangeRequest{
					Range: &docs.Range{
						StartIndex: StartIndex, //structuralElement.StartIndex,
						EndIndex:   EndIndex,   //  structuralElement.EndIndex,
					},
				},
			}

			_, err := p.clientInfo.driveClientDocService.Documents.BatchUpdate(documentID, &docs.BatchUpdateDocumentRequest{
				Requests: []*docs.Request{deleteRequest},
			}).Context(ctx).Do()
			if err != nil {
				return fmt.Errorf("ошибка при удалении таблицы: %v", err)
			}
			return nil
		}
	}

	return errors.New("таблица не содержит необходимых данных для удаления")
}

func (p *DocumentWorker) updateTableCell(documentID string, tableIndex, rowIndex, columnIndex int64, newText, lastText string) error {
	ctx := context.Background()

	// Получаем документ
	doc, err := p.clientInfo.driveClientDocService.Documents.Get(documentID).Context(ctx).Do()
	if err != nil {
		return fmt.Errorf("ошибка получения документа: %v", err)
	}

	// Получаем структурный элемент (таблицу) по индексу
	table := doc.Body.Content[tableIndex].Table

	// Получаем структурный элемент (ячейку) по индексам строки и столбца
	cell := table.TableRows[rowIndex].TableCells[columnIndex]

	// Создаем запрос для обновления содержимого ячейки
	updateRequest := &docs.Request{
		UpdateParagraphStyle: &docs.UpdateParagraphStyleRequest{
			ParagraphStyle: &docs.ParagraphStyle{
				NamedStyleType: "NORMAL_TEXT",
			},
			Range: &docs.Range{
				StartIndex: cell.StartIndex,
				EndIndex:   cell.EndIndex,
			},
			Fields: "namedStyleType",
		},
	}

	// Выполняем запрос на обновление стиля параграфа
	_, err = p.clientInfo.driveClientDocService.Documents.BatchUpdate(documentID, &docs.BatchUpdateDocumentRequest{
		Requests: []*docs.Request{updateRequest},
	}).Context(ctx).Do()

	if err != nil {
		return fmt.Errorf("ошибка при обновлении текста в ячейке: %v", err)
	}

	// Создаем запрос для вставки текста в ячейку
	insertTextRequest := &docs.Request{
		InsertText: &docs.InsertTextRequest{
			Location: &docs.Location{
				Index: cell.StartIndex + 1,
			},
			Text: newText,
		},
	}

	// Выполняем запрос на вставку текста в ячейку
	_, err = p.clientInfo.driveClientDocService.Documents.BatchUpdate(documentID, &docs.BatchUpdateDocumentRequest{
		Requests: []*docs.Request{insertTextRequest},
	}).Context(ctx).Do()

	if err != nil {
		return fmt.Errorf("ошибка при вставке текста в ячейку: %v", err)
	}

	_, err = p.clientDrive.createComment(documentID, lastText, newText, 1, 0)
	if err != nil {
		log.Printf("Failed to create comment: %v", err)
	}

	return nil
}

// ***************************************************************************************************************************
// _____________________________COMMENT NOTE WORKER_________________________________________
// ***************************************************************************************************************************

func (p *DocumentWorker) getComment(documentID string) error {

	comments, err := p.clientDrive.getCommentsFromDriveFile(documentID)
	if err != nil {
		log.Fatalf("Unable to get comments: %v", err)
	}

	fmt.Println("Комментарии в таблице:")
	for _, comment := range comments {
		fmt.Printf("ID: %s, Автор: %s, Текст: %s\n", comment.Id, comment.Author.DisplayName, comment.Content)

		if comment.QuotedFileContent != nil {
			fmt.Printf("Текст, на который наложен комментарий: %s\n", comment.QuotedFileContent.Value)
		}
	}
	return nil
}

func (p *DocumentWorker) getAllTableContent(tableRows []*docs.TableRow) []string {
	var CurrentValue []string

	for _, row := range tableRows {
		for _, cell := range row.TableCells {
			cellContent := p.getTableCellContent(cell)
			// Append the cell content to CurrentValue
			CurrentValue = append(CurrentValue, cellContent)
		}
	}

	re := regexp.MustCompile(regexValue["dataTable"])

	// Print or process CurrentValue as needed
	fmt.Println("Значение таблицы")
	for _, value := range CurrentValue {
		fmt.Print(value)
		if value != "" {
			match := re.FindStringSubmatch(value)

			if len(match) > 1 {

				//add comment
			}
		}
	}
	return CurrentValue
}

func (p *DocumentWorker) getTableCellContent(cell *docs.TableCell) string {
	var cellContent string

	for _, element := range cell.Content {
		cellContent += p.getStructuralElementContent(element)
	}

	// Add a comma between cells
	cellContent += ", "

	return cellContent
}

func (p *DocumentWorker) getStructuralElementContent(element *docs.StructuralElement) string {
	var content string

	if element.Paragraph != nil {
		content += p.getParagraphContent(element.Paragraph)
	} else if element.Table != nil {
		content += p.getCellTableContent(element.Table.TableRows)
	}

	return content
}

func (p *DocumentWorker) getParagraphContent(paragraph *docs.Paragraph) string {
	var content string

	for _, run := range paragraph.Elements {
		content += run.TextRun.Content
	}

	// Add a newline after each paragraph
	content += "\n"

	return content
}

func (p *DocumentWorker) getCellTableContent(tableRows []*docs.TableRow) string {
	var content string

	for _, row := range tableRows {
		for _, cell := range row.TableCells {
			content += p.getTableCellContent(cell)
		}
		// Add a newline after each row
		content += "\n"
	}

	return content
}

func (p *DocumentWorker) parseDocument(documentID string) error {
	// Получаем контент документа
	doc, err := p.clientInfo.driveClientDocService.Documents.Get(documentID).Do()
	if err != nil {
		return fmt.Errorf("ошибка при получении контента документа: %v", err)
	}

	var CurrentValue []string
	re := regexp.MustCompile(regexValue["dataTable"])

	for _, element := range doc.Body.Content {
		if element.Paragraph != nil {
			for _, run := range element.Paragraph.Elements {
				if run.TextRun != nil {
					fmt.Println(run.TextRun.Content)
					if run.TextRun.Content != "" {
						CurrentValue = append(CurrentValue, fmt.Sprint(run.TextRun.Content))

						match := re.FindStringSubmatch(run.TextRun.Content)

						if len(match) > 1 {

							//add comment
							matchValue := match[0]
							fmt.Printf("matchValue  %s\n", matchValue)

							_, err := p.clientDrive.createComment(documentID, matchValue, matchValue, 1, 0)
							if err != nil {
								log.Printf("Failed to create comment: %v", err)
							}

						}
					}

				}
			}
		}
		if element.Table != nil {
			// Print the table content
			fmt.Println("Значение таблицы")
			tableData := p.getAllTableContent(element.Table.TableRows)

			CurrentValue = append(CurrentValue, tableData...)

		}
	}
	fmt.Println("_________getComment_________")
	p.getComment(documentID)

	return nil
}

func (p *DocumentWorker) processDocument(documentID, newSpreadsheetID string, objectContent map[string]interface{}, mapCell map[string][]string) (string, error) {

	//p.parseDocument(documentID)

	// log.Fatalf("STOP")

	var mapTable = map[string]int{
		"*{Номер*}": 0,
		"*{Номенклатура.Наименование*}":  1,
		"*{Количество*}":                 2,
		"*{Единица измерения*}":          3,
		"*{Номенклатура.Размер.Ширина*}": 4,
	}

	fmt.Println("__________________FIND/INSERT TABLE__________________")
	doc, indexTable, StartIndex, EndIndex, err := p.findTableInDocument(documentID)
	if err != nil {
		log.Printf("ошибка findTableInDocument %v\n", err)
	}
	fmt.Printf("doc %v %v %v \n", doc, StartIndex, EndIndex)

	re := regexp.MustCompile(regexValue["dataTable"])

	for key, _ := range mapCell {
		fmt.Printf("mapCell key  %v     \n", key)

		match := re.FindStringSubmatch(key)

		if len(match) > 1 {

			expression := match[1]
			lastValue := match[0]
			_, valueJSON, found := p.findKeyInJSON(objectContent, expression)

			if found {
				fmt.Println("___________FIND valueJSON", valueJSON)
				fmt.Println("___________FIND key", expression)
				fmt.Println("___________lastValue", lastValue)

				for i, value := range valueJSON {
					if i == 0 {
						p.replaceTextDocument(documentID, lastValue, value)
					} else {
						numberColumn, ok := mapTable[key]
						if ok {
							err = p.updateTableCell(documentID, indexTable, 3, int64(numberColumn), value, lastValue)
							if err != nil {
								log.Printf("ошибка updateTableCell %v\n", err)
							}
						}
					}
				}
			}
		}
	}

	fmt.Println("__________________FIND/INSERT IMAGE__________________")

	_, valueJSON, found := p.findKeyInJSON(objectContent, "Картинки")
	fmt.Println("__valueJSON Картинки", valueJSON)

	if found {
		fmt.Println("__valueJSON Картинки", valueJSON)
		p.replaceImageInDocument(documentID, valueJSON)
	}

	fmt.Println("__________________Convert to PDF__________________")
	filename := "exported_doc1.pdf"
	urlNewFile, err := p.convertDocToPDF(documentID, PATH_FOLDER, filename)

	return urlNewFile, nil
}

// ***************************************************************************************************************************
// _____________________________Convert to PDF_________________________________________
// ***************************************************************************************************************************
// use alg https://spreadsheet.dev/comprehensive-guide-export-google-sheets-to-pdf-excel-csv-apps-script
// template https://docs.google.com/document/d/1XsrXzRWRKCyx2h4RKW0WPateugMpS2QY73MDUTOC00Y/edit
// -> https://docs.google.com/document/d/<documentID>/edit
// -> https://docs.google.com/document/d/<documentID>/export?format=pdf
// format : установить альбомную ориентацию страницы
// https://docs.google.com/spreadsheets/d/<documentID>/export?format=pdf&portrait=false
//  Например, предположим, что вы хотите экспортировать document в формате PDF в альбомной ориентации, без линий сетки, используя размер страницы B5
// https://docs.google.com/spreadsheets/d/<documentID>/export?format=pdf&portrait=false&gridlines=false&size=b5
/*
1. Функция getFileAsBlob()принимает URL-адрес в качестве входных данных и возвращает файл в виде большого двоичного объекта.
2. Назовите экспортированный файл
3. Сохраните экспортированный файл на Google Диск DriveApp.createFile(blob)
*/

var templateUrlDocument = map[string]string{
	"url":       "https://docs.google.com/document/d/",
	"export":    "/export",
	"pdf":       "?format=pdf",
	"landscape": "&portrait=false",
	"grid":      "&gridlines=false",
	"size_page": "&size=b5",
}

func (p *DocumentWorker) getFileAsBlob(requestURL string) error {
	// Make an HTTP GET request to the specified URL
	response, err := http.Get(requestURL)
	if err != nil {
		return fmt.Errorf("failed to make HTTP request: %w", err)
	}
	defer response.Body.Close()

	// Read the response body into a byte slice
	p.DocFileByteArray, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("failed to read response body: %w", err)
	}

	return nil
}

func (p *DocumentWorker) saveFileToGoogleDrive(filename, folderID string) (string, error) {
	// Use the Drive API client to create the file
	file := &drive.File{
		Name:    filename,           // Set the desired name for the file
		Parents: []string{folderID}, // Set the ID of the target folder
	}

	// Create a new file in Google Drive and upload the content
	createFile, err := p.clientInfo.driveClientService.Files.Create(file).Media(bytes.NewReader(p.DocFileByteArray)).Do()
	if err != nil {
		return "", fmt.Errorf("failed to create file in Google Drive: %w", err)
	}

	fmt.Printf("File '%s' created in Google Drive with ID: %s\n", filename, createFile.Id)

	return createFile.Id, nil
}

func (p *DocumentWorker) convertDocToPDF(documentID, outputPath, filename string) (string, error) {

	responseURL := templateUrlDocument["url"] + documentID + templateUrlDocument["export"] + templateUrlDocument["pdf"]

	err := p.getFileAsBlob(responseURL)
	if err != nil {
		return "", fmt.Errorf("failed to get file as blob: %w", err)
	}

	// Specify the desired filename for the exported PDF file
	idFolder, err := p.clientDrive.GetFolderID(outputPath)
	if err != nil {
		log.Printf("ошибка при поиске папки: %v", err)

		return "", fmt.Errorf("ошибка при поиске папки: %v", err)
	}
	fmt.Printf("idPATH_FOLDER: %s\n", idFolder)

	// Save the file to Google Drive
	id, err := p.saveFileToGoogleDrive(filename, idFolder)
	if err != nil {
		return "", fmt.Errorf("failed to save file to Google Drive: %w", err)
	}

	responseUrl := "https://drive.google.com/file/d/" + id + "/view"

	return responseUrl, nil
}
