package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"regexp"
	"strconv"
	"unicode"

	"google.golang.org/api/drive/v3"
	"google.golang.org/api/sheets/v4"
)

var regexValue = map[string]string{
	"dataTable": `\*\{(.*?)\*\}`,
}

type TableWorker struct {
	TableName          string
	Column             string
	Row                string
	spreadsheetID      string
	TableFileByteArray []byte

	clientInfo  *ClientInfo
	clientDrive *GoogleDriveWorker
}

func (p *TableWorker) getSpreadsheetIDByName(spreadsheetName string) (string, error) {
	// Get all files in Google Drive
	fileList, err := p.clientInfo.driveClientService.Files.List().Do()
	if err != nil {
		return "", fmt.Errorf("error retrieving file list: %w", err)
	}

	// Search for the spreadsheet by name
	for _, file := range fileList.Files {
		fmt.Printf("file.Name : %s\n", file.Name)
		if file.Name == spreadsheetName {
			return file.Id, nil
		}
	}

	return "", fmt.Errorf("spreadsheet not found: %s", spreadsheetName)
}

func (p *TableWorker) getSheetIDByName(spreadsheetID, sheetName string) (int64, error) {
	// Получаем информацию о таблице
	fmt.Println("Получаем информацию о таблице %s", spreadsheetID)
	spreadsheet, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Get(spreadsheetID).Do()
	if err != nil {
		return 0, fmt.Errorf("ошибка при получении информации о таблице: %w", err)
	}
	fmt.Println("Ищем лист с заданным именем")
	// Ищем лист с заданным именем
	for _, sheet := range spreadsheet.Sheets {
		fmt.Println(sheet.Properties.Title)
		if sheet.Properties.Title == sheetName {
			return sheet.Properties.SheetId, nil
		}
	}

	// Если лист с заданным именем не найден, возвращаем ошибку
	return 0, fmt.Errorf("лист с именем %s не найден в таблице %s", sheetName, spreadsheetID)
}

// **************************************************
// _______________COPE/ PASTE PAGE TABLE_______________
// **************************************************

// CopyTo метод использует идентификатор исходной таблицы и идентификатор листа внутри этой таблицы. Копия листа затем создается в той же самой таблице, что и исходный лист.
func (p *TableWorker) copySheet(sourceSpreadsheetID, sourceSheetID string) (int64, error) {
	// Create a request to copy the sheet
	copyRequest := &sheets.CopySheetToAnotherSpreadsheetRequest{}

	// Convert sourceSheetID to int64
	sheetIDInt, err := strconv.ParseInt(sourceSheetID, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("error converting sourceSheetID to int64: %w", err)
	}

	// Execute the copy request
	response, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Sheets.CopyTo(sourceSpreadsheetID, sheetIDInt, copyRequest).Do()
	if err != nil {
		return 0, fmt.Errorf("error copying sheet: %w", err)
	}

	// Get the ID of the new sheet
	newSheetID := response.SheetId
	fmt.Printf("newSheetID: %d\n", newSheetID)
	return newSheetID, nil
}

func (p *TableWorker) createTemplateSpreadsheet(templateIDSpreadsheet, newTitle, newParentFolderID string) (string, error) {
	// Retrieve template spreadsheet by its ID
	fileID := templateIDSpreadsheet
	originalFile, err := p.clientInfo.driveClientService.Files.Get(fileID).Do()
	if err != nil {
		return "", fmt.Errorf("unable to get template spreadsheet: %w", err)
	}

	// Create a new file based on the original
	newFile := &drive.File{
		Name:    newTitle,
		Parents: []string{newParentFolderID}, // Set the ID of the new parent folder
	}

	// Call Google Drive API to copy the file
	resp, err := p.clientInfo.driveClientService.Files.Copy(fileID, newFile).Do()
	if err != nil {
		return "", fmt.Errorf("unable to copy file: %w", err)
	}

	// Optionally, you can remove the original parent to effectively move the file
	_, err = p.clientInfo.driveClientService.Files.Update(resp.Id, nil).RemoveParents(strings.Join(originalFile.Parents, ",")).Do()
	if err != nil {
		return "", fmt.Errorf("unable to remove original parent: %w", err)
	}

	return resp.Id, nil
}

// **************************************************
// _______________variables and cells_______________
// **************************************************

func (p *TableWorker) updateCellValue(spreadsheetID, sheetName, cell, newValue string) error {
	valueRange := &sheets.ValueRange{
		Values: [][]interface{}{{newValue}},
	}

	fmt.Println("updateCellValue valueRange", valueRange) //sheetName+"!"+cell
	fmt.Println("updateCellValue cell", cell)

	_, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Update(spreadsheetID, cell, valueRange).
		ValueInputOption("RAW").Do()

	if err != nil {
		return fmt.Errorf("unable to update cell value: %v", err)
	}

	return nil
}

func (p *TableWorker) updateCellValueWithFormatting(spreadsheetID, newValue string, rowIndexStart, columnIndexStart, rowIndexEnd, columnIndexEnd, sheetID int64) error {
	reqs := []*sheets.Request{
		&sheets.Request{
			UpdateCells: &sheets.UpdateCellsRequest{
				Range: &sheets.GridRange{
					SheetId:          sheetID,
					StartColumnIndex: 2,
					StartRowIndex:    6,
					EndColumnIndex:   3,
					EndRowIndex:      7,
				},
				Rows: []*sheets.RowData{
					&sheets.RowData{
						Values: []*sheets.CellData{
							&sheets.CellData{
								UserEnteredValue: &sheets.ExtendedValue{
									StringValue: &newValue,
								},
							},
						},
					},
				},
				Fields: "userEnteredValue", // Указываем, какие поля обновляем (здесь только значение)
			},
		},
	}

	rb := &sheets.BatchUpdateSpreadsheetRequest{
		Requests: reqs,
	}

	_, err := p.clientInfo.driveClientSheetsService.Spreadsheets.BatchUpdate(spreadsheetID, rb).Context(context.Background()).Do()
	if err != nil {
		return fmt.Errorf("unable to update cell value with formatting: %v", err)
	}

	return nil
}

func (p *TableWorker) mergeCells(spreadsheetID, sheetName string, startRowIndex, endRowIndex, startColumnIndex, endColumnIndex int) error {
	// Create a request to merge cells
	request := &sheets.BatchUpdateSpreadsheetRequest{
		Requests: []*sheets.Request{
			{
				MergeCells: &sheets.MergeCellsRequest{
					Range: &sheets.GridRange{
						SheetId:          0, // Assuming the first sheet, change if needed
						StartRowIndex:    int64(startRowIndex),
						EndRowIndex:      int64(endRowIndex),
						StartColumnIndex: int64(startColumnIndex),
						EndColumnIndex:   int64(endColumnIndex),
					},
					MergeType: "MERGE_ALL", // Adjust merge type as needed
				},
			},
		},
	}

	// Execute the merge request
	_, err := p.clientInfo.driveClientSheetsService.Spreadsheets.BatchUpdate(spreadsheetID, request).Do()
	if err != nil {
		return fmt.Errorf("error merging cells: %w", err)
	}

	return nil
}

func (p *TableWorker) unmergeCells(spreadsheetID, sheetName string, startRowIndex, endRowIndex, startColumnIndex, endColumnIndex int) error {
	// Create a request to unmerge cells
	request := &sheets.BatchUpdateSpreadsheetRequest{
		Requests: []*sheets.Request{
			{
				UnmergeCells: &sheets.UnmergeCellsRequest{
					Range: &sheets.GridRange{
						SheetId:          0, // Assuming the first sheet, change if needed
						StartRowIndex:    int64(startRowIndex),
						EndRowIndex:      int64(endRowIndex),
						StartColumnIndex: int64(startColumnIndex),
						EndColumnIndex:   int64(endColumnIndex),
					},
				},
			},
		},
	}

	// Execute the unmerge request
	_, err := p.clientInfo.driveClientSheetsService.Spreadsheets.BatchUpdate(spreadsheetID, request).Do()
	if err != nil {
		return fmt.Errorf("error unmerging cells: %w", err)
	}

	return nil
}

// **************************************************
// _____________Insert/delete Row_________________
// **************************************************

func (p *TableWorker) incrementRowInCellKey(cellKey string, rowStart int) (string, int, error) {

	parts := strings.Split(cellKey, "!")
	if len(parts) != 2 {
		return "", 0, fmt.Errorf("некорректный формат ключа ячейки: %s", cellKey)
	}

	sheetName := parts[0]
	cellRange := parts[1]
	newRow := 0
	// Разделяем диапазон ячеек, если он указан
	cellRangeParts := strings.Split(cellRange, ":")
	for i, cell := range cellRangeParts {
		// Разбиваем строку ячейки на части (буквы и цифры)
		letterPart := ""
		numberPart := ""
		for _, char := range cell {
			if unicode.IsLetter(char) {
				letterPart += string(char)
			} else if unicode.IsDigit(char) {
				numberPart += string(char)
			}
		}

		// Преобразуем строку с цифрами в число
		number, err := strconv.Atoi(numberPart)
		if err != nil {
			return "", 0, fmt.Errorf("невозможно преобразовать строку с цифрами в число: %v", err)
		}

		if number < rowStart {
			return cellKey, number, nil
		}
		// Увеличиваем число на 1
		number++
		newRow = number
		// Собираем обратно строку ячейки
		cellRangeParts[i] = fmt.Sprintf("%s%d", letterPart, number)
	}

	// Собираем обратно строку ключа ячейки
	updatedCellKey := fmt.Sprintf("%s!%s", sheetName, strings.Join(cellRangeParts, ":"))

	return updatedCellKey, newRow, nil
}

func (p *TableWorker) insertEmptyRow(spreadsheetID, sheetName string, rowIndex int) error {
	// Формируем запрос для вставки строки.
	SheetId, err := p.getSheetIDByName(spreadsheetID, sheetName)
	if err != nil {
		return fmt.Errorf("Unable to get sheet ID: %v", err)
	}

	// Формируем запрос для вставки строки.
	requests := []*sheets.Request{
		{
			InsertDimension: &sheets.InsertDimensionRequest{
				Range: &sheets.DimensionRange{
					SheetId:    SheetId,
					Dimension:  "ROWS",
					StartIndex: int64(rowIndex - 1),
					EndIndex:   int64(rowIndex),
				},
				InheritFromBefore: false,
			},
		},
	}

	// Выполняем запрос.
	_, err = p.clientInfo.driveClientSheetsService.Spreadsheets.BatchUpdate(spreadsheetID, &sheets.BatchUpdateSpreadsheetRequest{
		Requests: requests,
	}).Do()

	return err
}

func (p *TableWorker) findCell(spreadsheetID, rangeToUpdate, keyData string) (string, error) {
	resp, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Get(spreadsheetID, rangeToUpdate).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet: %v", err)
		return "", err
	}

	// Ищем ячейку с ключевыми данными
	for rowIdx, row := range resp.Values {
		for colIdx, cell := range row {
			if cell == keyData {
				// Возвращаем адрес найденной ячейки
				return fmt.Sprintf("%s%d", p.getColumnIndex(colIdx+1), rowIdx+1), nil
			}
		}
	}

	return "", fmt.Errorf("Key data '%s' not found in the specified range", keyData)
}

func (p *TableWorker) getColumnIndex(index int) string {
	// Преобразование номера колонки в буквенное представление
	var result string
	for {
		index--
		result = string(byte('A'+index%26)) + result
		index /= 26
		if index == 0 {
			break
		}
	}
	return result
}

func (p *TableWorker) getHeaders(spreadsheetID, sheetName string, startRowIndex int, endRowIndex int) ([]string, error) {
	// Specify the range to get values, starting from the provided row index
	rangeStr := fmt.Sprintf("%s!A%d:G%d", sheetName, startRowIndex, endRowIndex)
	fmt.Printf("getHeaders rangeStr %s\n", rangeStr)
	// Execute the request to get values from the specified range
	response, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Get(spreadsheetID, rangeStr).Do()
	if err != nil {
		return nil, fmt.Errorf("error getting values: %w", err)
	}

	// Check if any values are returned
	if len(response.Values) == 0 {
		return nil, fmt.Errorf("no values found in the specified range")
	}

	// Extract header names from the first row of the range
	headers := response.Values[0]
	fmt.Printf("getHeaders headers %s\n", headers)

	// Convert header names to strings
	var headerStrings []string
	for _, header := range headers {
		if headerString, ok := header.(string); ok {
			headerStrings = append(headerStrings, headerString)
		}
	}

	return headerStrings, nil
}

func (p *TableWorker) reverseString(s string) string {
	// Обращение строки
	var result strings.Builder
	for i := len(s) - 1; i >= 0; i-- {
		result.WriteByte(s[i])
	}
	return result.String()
}

func (p *TableWorker) insertDataIntoTable(spreadsheetID, sheetName string, rowValues []interface{}, rangeVal string) error {

	// Specify the range
	rangeValSheets := fmt.Sprintf("%s!%s", sheetName, rangeVal)

	// Get current values in the specified range
	resp, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Get(spreadsheetID, rangeValSheets).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet: %v", err)
		return err
	}

	fmt.Println("insertDataIntoTable resp %s", resp)

	// Insert a new row with the values
	var values [][]interface{}
	if len(resp.Values) > 0 {
		// If there are existing values, insert the new row below them
		values = append(resp.Values, rowValues)
	} else {
		// If the range is empty, simply insert the new row
		values = append(values, rowValues)
	}

	// Update the values in the specified range
	_, err = p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Update(spreadsheetID, rangeValSheets, &sheets.ValueRange{
		Values: values,
	}).ValueInputOption("USER_ENTERED").Do()
	if err != nil {
		log.Fatalf("Unable to update data in sheet: %v", err)
		return err
	}

	fmt.Printf("Inserted data at %s!\n", rangeValSheets)
	return nil
}

// 1-A 2-B
func (p *TableWorker) ColumnIndexToLetter(index int) string {
	// Преобразование номера колонки в буквенное представление
	var result strings.Builder
	for {
		index--
		result.WriteByte(byte('A' + index%26))
		index /= 26
		if index == 0 {
			break
		}
	}
	return p.reverseString(result.String())
}

// Вспомогательная функция для печати содержимого ячеек
func (p *TableWorker) printCellValues(response *sheets.ValueRange) {
	if len(response.Values) == 0 {
		fmt.Println("Нет данных в ячейках.")
		return
	}

	for _, row := range response.Values {
		for _, value := range row {
			fmt.Printf("___printCellValues  %v\t", value)
		}
		fmt.Println()
	}
	fmt.Println()
}

// ***************************************************************************************************************************
// _____________________________COMMENT NOTE WORKER_________________________________________
// ***************************************************************************************************************************

func (p *TableWorker) getCellNote(fileID, sheetName, cellRange string) (map[string]string, error) {

	var notes = make(map[string]string)

	// Выполняем запрос к Google Sheets API
	resp, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Get(fileID).
		Ranges(cellRange).
		IncludeGridData(true).
		Do()
	if err != nil {
		return notes, err
	}
	fmt.Println("resp")
	fmt.Println(resp)
	// Проверяем наличие данных в ответе
	if len(resp.Sheets) > 0 && len(resp.Sheets[0].Data) > 0 && len(resp.Sheets[0].Data[0].RowData) > 0 {

		// Итерируем по строкам и столбцам
		for rowIndex, rowData := range resp.Sheets[0].Data[0].RowData {
			if len(rowData.Values) > 0 {
				for colIndex, cell := range rowData.Values {
					// Проверяем наличие заметки в ячейке
					if cell.Note != "" {

						convertColumn := p.ColumnIndexToLetter(colIndex + 1)
						cellAddress := fmt.Sprintf("%s!%s%d", sheetName, convertColumn, rowIndex+1)
						fmt.Println("sheet cellAddress")
						fmt.Println(cellAddress)
						notes[cellAddress] = cell.Note
					}
				}
			}
		}
		fmt.Println("sheet notes")
		fmt.Println(notes)

		return notes, nil
	}

	return notes, fmt.Errorf("No notes found in the specified range")
}

func (p *TableWorker) AddNoteToMergedCells(spreadsheetID, noteVal string, rowIndexStart, columnIndexStart, rowIndexEnd, columnIndexEnd, sheetID int64) error {
	// Формируем запрос на обновление ячейки
	reqs := []*sheets.Request{
		&sheets.Request{
			UpdateCells: &sheets.UpdateCellsRequest{
				Range: &sheets.GridRange{
					SheetId:          sheetID,
					StartColumnIndex: columnIndexStart,
					StartRowIndex:    rowIndexStart,
					EndColumnIndex:   columnIndexEnd,
					EndRowIndex:      rowIndexEnd,
				},
				Rows: []*sheets.RowData{
					&sheets.RowData{
						Values: []*sheets.CellData{
							&sheets.CellData{
								Note: noteVal,
							},
						},
					},
				},
				Fields: "note",
			},
		},
	}

	// Формируем запрос на batchUpdate
	rb := &sheets.BatchUpdateSpreadsheetRequest{
		Requests: reqs,
	}

	// Выполняем запрос
	_, err := p.clientInfo.driveClientSheetsService.Spreadsheets.BatchUpdate(spreadsheetID, rb).Context(context.Background()).Do()
	if err != nil {
		return fmt.Errorf("Unable to add note to merged cells: %v", err)
	}

	return nil
}

func (p *TableWorker) AddNoteToCell(spreadsheetID, noteVal string, sheetID, rowIndex, columnIndex int64) error {
	// Формируем запрос на обновление ячейки
	reqs := []*sheets.Request{
		&sheets.Request{
			UpdateCells: &sheets.UpdateCellsRequest{
				Start: &sheets.GridCoordinate{
					SheetId:     sheetID,     // Идентификатор листа
					RowIndex:    rowIndex,    // Начальная строка
					ColumnIndex: columnIndex, // Начальный столбец
				},
				Rows: []*sheets.RowData{
					&sheets.RowData{
						Values: []*sheets.CellData{
							&sheets.CellData{
								Note: noteVal,
							},
						},
					},
				},
				Fields: "note", // Указываем, какие поля обновляем
			},
		},
	}

	// Формируем запрос на batchUpdate
	rb := &sheets.BatchUpdateSpreadsheetRequest{
		Requests: reqs,
	}

	// Выполняем запрос
	_, err := p.clientInfo.driveClientSheetsService.Spreadsheets.BatchUpdate(spreadsheetID, rb).Context(context.Background()).Do()
	if err != nil {
		return fmt.Errorf("Unable to add note to cell: %v", err)
	}

	return nil
}

func (p *TableWorker) findMergeCell(spreadsheetID string, sheetID int64) (map[string][]string, map[string][][]int64, error) {

	var mapMergeCell = make(map[string][]string)
	var mapCellCoordinat = make(map[string][][]int64)

	// Получение информации о листе
	resp, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Get(spreadsheetID).Do()
	if err != nil {
		log.Fatal(err)
	}

	// Печать информации о объединенных ячейках и их содержимом
	for _, sheet := range resp.Sheets {
		for _, mergeCellRange := range sheet.Merges {

			// Получение содержимого объединенных ячеек
			convertColumnStart := p.ColumnIndexToLetter(int(mergeCellRange.StartColumnIndex) + 1)
			convertColumnEnd := p.ColumnIndexToLetter(int(mergeCellRange.EndColumnIndex))

			valuesRange := fmt.Sprintf("%s!%s%d:%s%d", sheet.Properties.Title, convertColumnStart, mergeCellRange.StartRowIndex+1, convertColumnEnd, mergeCellRange.EndRowIndex)
			valuesResponse, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Get(spreadsheetID, valuesRange).Do()
			if err != nil {
				log.Printf("Ошибка при получении значений из ячеек: %v", err)
			} else {
				for _, row := range valuesResponse.Values {
					for _, value := range row {
						key := fmt.Sprintf("%v", value)
						mapMergeCell[key] = append(mapMergeCell[key], valuesRange)

						newValue := []int64{int64(mergeCellRange.StartRowIndex), int64(mergeCellRange.EndRowIndex), int64(mergeCellRange.StartColumnIndex), int64(mergeCellRange.EndColumnIndex)}
						mapCellCoordinat[key] = append(mapCellCoordinat[key], newValue)

						re := regexp.MustCompile(regexValue["dataTable"])
						match := re.FindStringSubmatch(key)
						if len(match) > 1 {

							err = p.AddNoteToMergedCells(spreadsheetID, key, mergeCellRange.StartRowIndex, mergeCellRange.StartColumnIndex, mergeCellRange.EndRowIndex, mergeCellRange.EndColumnIndex, sheetID)
							if err != nil {
								log.Printf("ERROR AddNoteToMergedCells %v", err)
								return mapMergeCell, mapCellCoordinat, fmt.Errorf("ERROR AddNoteToMergedCells: %v", err)
							}
						}
					}
				}

			}
		}
	}

	return mapMergeCell, mapCellCoordinat, err

}

func (p *TableWorker) findAllCell(spreadsheetID, rangeStr, sheetName string, mapCell *map[string][]string, mapCellCoordinat *map[string][][]int64, sheetID int64) error {

	response, err := p.clientInfo.driveClientSheetsService.Spreadsheets.Values.Get(spreadsheetID, rangeStr).Do()
	if err != nil {
		return fmt.Errorf("error getting values: %w", err)
	}

	// Check if any values are returned
	if len(response.Values) == 0 {
		return fmt.Errorf("no values found in the specified range")
	}

	for rowNumber, row := range response.Values {

		for numberColumn, value := range row {
			if value != nil {

				stringValue, ok := value.(string)
				if !ok {
					return fmt.Errorf("невозможно преобразовать значение в строку: %v", value)
				}

				if value != "" {
					/*
					 *{Номер*}-[E1:G1 D12]
					 *{Количество*}-[C7]
					 */
					var arrMap []string
					for key, valueMap := range *mapCell { //string - []string
						if key == stringValue {
							arrMap = valueMap
							break
						}
					}
					convertColumnStart := p.ColumnIndexToLetter(int(numberColumn) + 1)
					valuesRange := fmt.Sprintf("%s!%s%d", sheetName, convertColumnStart, rowNumber+1)

					if len(arrMap) > 0 {
						for _, valuemap := range arrMap {
							if strings.Contains(valuemap, valuesRange) {
							} else {
								(*mapCell)[stringValue] = append((*mapCell)[stringValue], valuesRange)

								newValue := []int64{int64(rowNumber), int64(rowNumber + 1), int64(numberColumn), int64(numberColumn + 1)}
								(*mapCellCoordinat)[stringValue] = append((*mapCellCoordinat)[stringValue], newValue)

								re := regexp.MustCompile(regexValue["dataTable"])
								match := re.FindStringSubmatch(stringValue)
								if len(match) > 1 {
									p.AddNoteToCell(spreadsheetID, stringValue, sheetID, int64(rowNumber), int64(numberColumn))
								}
							}
						}
					} else {

						(*mapCell)[stringValue] = append((*mapCell)[stringValue], valuesRange)

						newValue := []int64{int64(rowNumber), int64(rowNumber + 1), int64(numberColumn), int64(numberColumn + 1)}
						(*mapCellCoordinat)[stringValue] = append((*mapCellCoordinat)[stringValue], newValue)

						re := regexp.MustCompile(regexValue["dataTable"])
						match := re.FindStringSubmatch(stringValue)
						if len(match) > 1 {
							p.AddNoteToCell(spreadsheetID, stringValue, sheetID, int64(rowNumber), int64(numberColumn))
						}
					}
				}
			}
		}
	}

	return err

}

func (p *TableWorker) findKeyInJSON(data map[string]interface{}, nameKey string) (string, []string, bool) {

	fmt.Println("__________findKeyInJSON__________nameKey", nameKey)
	answerValue := []string{}
	findValue := false

	for key, value := range data {
		fmt.Printf("__________findKeyInJSON  value %v , key %v\n", value, key)
		switch v := value.(type) {
		case string:
			fmt.Printf(" case string: =%v  %v\n", key, nameKey)

			if key == nameKey {
				// Найден ключ, возвращаем его и значение
				answerValue = append(answerValue, fmt.Sprintf("%v", value))
				findValue = true
				fmt.Printf("НАШЛИ case string: =%v\n", answerValue)
				//return key, answerValue, true
			}
		case int:
			fmt.Printf(" case int: =%v  %v\n", key, nameKey)

			if key == nameKey {
				// Найден ключ, возвращаем его и значение
				answerValue = append(answerValue, fmt.Sprintf("%v", value))
				findValue = true
				fmt.Printf("НАШЛИ case int: =%v\n", answerValue)
			}
		case float64:
			fmt.Printf(" case float64: =%v  %v\n", key, nameKey)

			if key == nameKey {
				// Найден ключ, возвращаем его и значение
				answerValue = append(answerValue, fmt.Sprintf("%v", value))
				findValue = true
				fmt.Printf("НАШЛИ case float64: =%v\n", answerValue)
			}
		case []interface{}:

			fmt.Println(value)
			flagContains := false
			var firstKey string
			var lastKey string

			if strings.Contains(nameKey, ".") {
				fmt.Println(nameKey)
				partsKey := strings.Split(nameKey, ".") //[]string
				flagContains = true
				firstKey = partsKey[0]
				lastKey = strings.Join(partsKey[1:], ".")

			} else {
				fmt.Println(nameKey)
				firstKey = ""
				lastKey = nameKey

			}
			if key == firstKey || flagContains == false {
				for _, item := range v {
					fmt.Printf("__________findKeyInJSON  item %v  \n", item)

					nestedObj, ok := item.(map[string]interface{})
					if !ok {
						continue
					}
					fmt.Printf("__________findKeyInJSON  nestedObj %v  \n", nestedObj)

					for nestedKey, nestedValue := range nestedObj {
						fmt.Printf("__________findKeyInJSON  nestedKey %v , nestedValue %v\n", nestedKey, nestedValue)

						if nestedKey == lastKey {
							// Найден ключ, возвращаем его и значение
							answerValue = append(answerValue, fmt.Sprintf("%v", nestedValue))
							fmt.Printf("НАШЛИ answerValue =%v\n", answerValue)
							findValue = true
						}
					}
				}
				//return lastKey, answerValue, true
			}
		case map[string]interface{}:

			flagContains := false
			var firstKey string
			var lastKey string

			if strings.Contains(nameKey, ".") {

				partsKey := strings.Split(nameKey, ".") //[]string
				flagContains = true
				firstKey = partsKey[0]
				lastKey = strings.Join(partsKey[1:], ".")

			} else {
				firstKey = ""
				lastKey = nameKey

			}

			if key == firstKey || flagContains == false {
				for nestedKey, nestedValue := range v {
					if nestedKey == lastKey {
						// Найден ключ, возвращаем его и значение
						answerValue = append(answerValue, fmt.Sprintf("%v", nestedValue))
						findValue = true
					}
				}
				//return lastKey, answerValue, true

			}
		default:
			fmt.Println("Неизвестный тип")

		}

	}
	// Ключ не найден
	return nameKey, answerValue, findValue
}

/*
*{Номер*}-[E1:G1 D12]
*{Количество*}-[C7]
 */

func (p *TableWorker) processTable(_sheetName, spreadsheetID string, objectContent map[string]interface{}, rangeStr string) (map[string][]string, string, error) {

	var mapCell = make(map[string][]string)
	var mapCellCoordinat = make(map[string][][]int64)

	idSheet, err := p.getSheetIDByName(spreadsheetID, _sheetName)
	if err != nil {
		return mapCell, "", fmt.Errorf("error getSheetIDByName: %w", err)
	}

	fmt.Println("__________________PARSE MERGE CELL__________________")

	mapCell, mapCellCoordinat, err = p.findMergeCell(spreadsheetID, idSheet)
	if err != nil {
		return mapCell, "", fmt.Errorf("error findMergeCell: %w", err)
	}

	fmt.Println("__________________PARSE CELL__________________")

	err = p.findAllCell(spreadsheetID, rangeStr, _sheetName, &mapCell, &mapCellCoordinat, idSheet)
	if err != nil {
		return mapCell, "", fmt.Errorf("error findAllCell: %w", err)
	}

	fmt.Println("***mapCellCoordinat***")
	fmt.Println(mapCellCoordinat)

	fmt.Println("__________________FIND/INSERT__________________")
	re := regexp.MustCompile(regexValue["dataTable"])
	var rowAddNumber int = 0

	for key, value := range mapCell {
		fmt.Printf("mapCell key  %v     value  %v\n", key, value)

		match := re.FindStringSubmatch(key)

		if len(match) > 1 {

			expression := match[1]

			_, valueJSON, found := p.findKeyInJSON(objectContent, expression)

			if found {

				for _, valueRangeTable := range value {
					if len(valueJSON) == 1 {
						p.updateCellValue(spreadsheetID, _sheetName, valueRangeTable, valueJSON[0])
						p.updateCellValueWithFormatting(spreadsheetID, "test", 6, 2, 6, 2, idSheet)
					} else {

						var iterKey string = valueRangeTable
						newRow := 0
						for i, arr := range valueJSON {
							fmt.Printf("ЦИКЛ ВСТАВКИ ЗАНЧЕНИЙ i %v, значение %v, место вставки %v \n", i, arr, iterKey)
							p.updateCellValue(spreadsheetID, _sheetName, iterKey, arr)

							if i < len(valueJSON)-1 {

								iterKey, newRow, err = p.incrementRowInCellKey(iterKey, 0)

								if rowAddNumber != newRow {

									err = p.insertEmptyRow(spreadsheetID, _sheetName, newRow)
									if err != nil {
										return mapCell, "", fmt.Errorf("error insertEmptyRow: %w", err)
									}
									rowAddNumber = newRow

									fmt.Println("__________________UPDATE MAP AFTER INSERT ROW__________________")
									for keyTemp, valueTemp := range mapCell {
										var updatedValues []string

										for _, vtmp := range valueTemp {
											newValueTemp, _, err := p.incrementRowInCellKey(vtmp, newRow)
											if err != nil {
												fmt.Println("Error:", err)
												continue
											}
											updatedValues = append(updatedValues, newValueTemp)

										}
										mapCell[keyTemp] = updatedValues
									}

								}
							}
						}

					}
				}
			}
		}

	}

	fmt.Println("__________________SAVE PDF__________________")

	filename := "exported_file.pdf"

	urlNewFile, err := p.convertSheetToPDF(spreadsheetID, PATH_FOLDER, _sheetName, filename, idSheet)

	return mapCell, urlNewFile, err
}

// ***************************************************************************************************************************
// _____________________________Convert to PDF_________________________________________
// ***************************************************************************************************************************
// use alg https://spreadsheet.dev/comprehensive-guide-export-google-sheets-to-pdf-excel-csv-apps-script
// template https://docs.google.com/spreadsheets/d/1SBaBlMkOczl4JzyAfdBMozI8JUfG_5i08klKYQ2DHa8/edit#gid=1903074578
// -> https://docs.google.com/spreadsheets/d/<SPREADSHEETID>/edit
// -> https://docs.google.com/spreadsheets/d/<SPREADSHEETID>/export?format=pdf
// format : установить альбомную ориентацию страницы
// https://docs.google.com/spreadsheets/d/<SPREADSHEETID>/export?format=pdf&portrait=false
//  Например, предположим, что вы хотите экспортировать электронную таблицу в формате PDF в альбомной ориентации, без линий сетки, используя размер страницы B5
// https://docs.google.com/spreadsheets/d/<SPREADSHEETID>/export?format=pdf&portrait=false&gridlines=false&size=b5
/*
1. Функция getFileAsBlob()принимает URL-адрес в качестве входных данных и возвращает файл в виде большого двоичного объекта.
2. Назовите экспортированный файл
3. Сохраните экспортированный файл на Google Диск DriveApp.createFile(blob)
*/

var templateUrlSpreadsheet = map[string]string{
	"url":       "https://docs.google.com/spreadsheets/d/",
	"export":    "/export",
	"pdf":       "?format=pdf",
	"landscape": "&portrait=false",
	"grid":      "&gridlines=false",
	"size_page": "&size=b5",
	"idSheet":   "&gid=",
}

func (p *TableWorker) getFileAsBlob(requestURL string) error {
	// Make an HTTP GET request to the specified URL
	response, err := http.Get(requestURL)
	if err != nil {
		return fmt.Errorf("failed to make HTTP request: %w", err)
	}
	defer response.Body.Close()

	// Read the response body into a byte slice
	p.TableFileByteArray, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("failed to read response body: %w", err)
	}

	return nil
}

func (p *TableWorker) saveFileToGoogleDrive(filename, folderID string) (string, error) {
	// Use the Drive API client to create the file
	file := &drive.File{
		Name:    filename,           // Set the desired name for the file
		Parents: []string{folderID}, // Set the ID of the target folder
	}

	// Create a new file in Google Drive and upload the content
	createFile, err := p.clientInfo.driveClientService.Files.Create(file).Media(bytes.NewReader(p.TableFileByteArray)).Do()
	if err != nil {
		return "", fmt.Errorf("failed to create file in Google Drive: %w", err)
	}

	fmt.Printf("File '%s' created in Google Drive with ID: %s\n", filename, createFile.Id)

	return createFile.Id, nil
}

func (p *TableWorker) convertSheetToPDF(spreadsheetID, outputPath, sheetName, filename string, sheetID int64) (string, error) {
	requestURL := templateUrlSpreadsheet["url"] + spreadsheetID + templateUrlSpreadsheet["export"] + templateUrlSpreadsheet["pdf"] + templateUrlSpreadsheet["idSheet"] + strconv.FormatInt(sheetID, 10)

	err := p.getFileAsBlob(requestURL)
	if err != nil {
		return "", fmt.Errorf("failed to get file as blob: %w", err)
	}

	// Specify the desired filename for the exported PDF file
	idFolder, err := p.clientDrive.GetFolderID(outputPath)
	if err != nil {
		log.Printf("ошибка при поиске папки: %v", err)

		return "", fmt.Errorf("ошибка при поиске папки: %v", err)
	}
	fmt.Printf("idPATH_FOLDER: %s\n", idFolder)

	// Save the file to Google Drive
	id, err := p.saveFileToGoogleDrive(filename, idFolder)
	if err != nil {
		return "", fmt.Errorf("failed to save file to Google Drive: %w", err)
	}
	responseUrl := "https://drive.google.com/file/d/" + id + "/view"
	return responseUrl, nil
}
