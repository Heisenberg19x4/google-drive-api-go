package main

import (
	"fmt"
	//"encoding/json"
	"log"
	"time"
	//"strings"
)

type JsonWorker struct {
	newSpreadsheetID  string
	newDocumentID     string
	sheetID           string
	sheetName         string
	tableWorker       *TableWorker
	documentWorker    *DocumentWorker
	googleDriveWorker *GoogleDriveWorker
	mapCell           map[string][]string
}

// **********************************************************************************
// _______________Methods JSON_______________
// **********************************************************************************

// **********************************************************************************
// _______________the scenario of working with the template_______________
// **********************************************************************************

var jsonData = `{
    "Сведения": {
        "СтрНачальная": "2",
        "Cодержание": [
            {
                "Заголовок": "Содержание",
                "НомерСтраницы": "2",
                "Cодержание": [
                    {
                        "Заголовок": "Заг второй уровень",
                        "НомерСтраницы": "2"
                    },
                    {
                        "Заголовок": "Заг второй уровень",
                        "НомерСтраницы": "3"
                    }
                ]
            },
            {
                "Заголовок": "Введение",
                "НомерСтраницы": "3"
            }
        ]
    },
    "Данные": {
        "Номенклатура": [
            {
                "Наименование": "Изделие №1",
                "Количество": "10",
                "Номер": "1#$;2;;1;1;",
                "Единица измерения": "шт",
                "Размер.Ширина": "10х10"
            },
            {
                "Наименование": "Изделие №2",
                "Количество": "10",
                "Единица измерения": "шт",
                "Размер.Ширина": "10х10"
            }
        ],
        "НомерАкта": 1, 
        "Дата": "22.01.2023",
        "Контрагент": {
            "Наименование": "Ип Иванов И.И.",
            "ИНН": "1234567890"
        },
        "Картинки": {
            "ФотоМое1": "https://img.freepik.com/premium-vector/family-person-together-at-cartoon-scene_109722-2876.jpg?w=1380",
            "ФотоМое2": "https://img.freepik.com/free-photo/wide-shot-of-river-going-through-the-forest-and-the-mountains-captured-in-kauai-hawaii_181624-7514.jpg?t=st=1702672221~exp=1702672821~hmac=9345082aec777bc5dc8c61ec2fb96f6b66f1897ba3f40020f23dad6feffc96c7"
        }
    }
}`

var PATH_TEMPLATE_FOLDER = "Шаблоны"
var DOC_TEMPLATE = "Модуль отчетов.Пример word"
var SHEET_TEMPLATE = "Модуль отчетов.Пример Excel"
var PATH_FOLDER = "Результат"

func (p *JsonWorker) scenarioTemplateDoc(documentID, newSpreadsheetID string, data map[string]interface{}) (string, error) {

	fmt.Println("____________________scenarioTemplateDoc_______________ \n")

	newUrlFile, err := p.documentWorker.processDocument(documentID, newSpreadsheetID, data, p.mapCell)
	if err != nil {
		log.Printf("ошибка scenarioTemplateDoc %v\n", err)
	}
	return newUrlFile, nil

}

func (p *JsonWorker) scenarioTemplateTable(nameSheet, spreadsheetID string, data map[string]interface{}) (string, error) {

	fmt.Println("____________________scenarioTemplateTable_______________ \n")

	rangeStr := fmt.Sprintf("%s!A%d:G%d", nameSheet, 1, 12)
	mapCell, newUrlFile, err := p.tableWorker.processTable(nameSheet, spreadsheetID, data, rangeStr)
	if err != nil {
		log.Printf("ошибка processTable %v\n", err)
	}
	p.mapCell = mapCell

	return newUrlFile, nil
}

func (p *JsonWorker) scenarioTemplate(document map[string]interface{}) (string, string, error) {

	var nameSheet = "Лист1"
	fmt.Println(nameSheet)
	idResultFolder, err := p.googleDriveWorker.GetFolderID(PATH_FOLDER)
	if err != nil {
		log.Printf("ошибка при поиске папки: %v", err)

		return "", "", fmt.Errorf("ошибка при поиске папки: %v", err)
	}
	fmt.Println("idResultFolder")
	fmt.Println(idResultFolder)

	idTemplateFolder, err := p.googleDriveWorker.GetFolderID(PATH_TEMPLATE_FOLDER)
	if err != nil {
		log.Printf("ошибка при поиске папки: %v", err)

		return "", "", fmt.Errorf("ошибка при поиске папки: %v", err)
	}
	fmt.Printf("idTemplateFolder: %s\n", idTemplateFolder)

	//GET id template TABLE
	idTable, err := p.tableWorker.getSpreadsheetIDByName(SHEET_TEMPLATE)
	if err != nil {
		return "", "", fmt.Errorf("error getSpreadsheetIDByName  %v", err)
	}
	fmt.Printf("idTable: %s\n", idTable)

	//GET id template DOCUMNET
	idDocument, err1 := p.documentWorker.GetDocumentIDByNameAndPath(DOC_TEMPLATE, idTemplateFolder)
	if err1 != nil {
		return "", "", fmt.Errorf("error GetDocumentIDByNameAndPath %v", err)
	}
	fmt.Printf("\nidDocument: %s\n", idDocument)

	currentTime := time.Now()

	// Форматируем текущее время в строку
	timeString := currentTime.Format("2006-01-02_15-04-05") // Здесь вы можете выбрать нужный вам формат

	newNameTemplate := make([]string, 2)
	// Создаем новое имя файла с временем
	newNameTemplate[0] = fmt.Sprintf("Копия шаблона_таблицы%s", timeString)
	newNameTemplate[1] = fmt.Sprintf("Копия шаблона_документа%s", timeString)

	fmt.Printf("newNameTemplate[0]: %s\n", newNameTemplate[0])
	fmt.Printf("newNameTemplate[1]: %s\n", newNameTemplate[1])

	// create template table
	newSpreadsheetID, err := p.tableWorker.createTemplateSpreadsheet(idTable, newNameTemplate[0], idResultFolder) //"1BPA9CgFd0IK-rM82gTaeAn7gYHl1dVnNRTfOVQZGFj0"//
	// create template documents
	newDocumentID, err := p.documentWorker.CopyDocumentTemplate(idDocument, newNameTemplate[1], idResultFolder)
	p.newDocumentID = newDocumentID
	p.newSpreadsheetID = newSpreadsheetID

	fmt.Printf("newDocumentID: %s\n", newDocumentID)
	fmt.Printf("newSpreadsheetID: %s\n", newSpreadsheetID)

	sheetID, err := p.tableWorker.getSheetIDByName(newSpreadsheetID, nameSheet)
	if err != nil {
		log.Fatalf("Ошибка при создании: %v", err)
	}
	p.sheetID = fmt.Sprintf("%d", sheetID)
	fmt.Printf("sheetID: %s\n", sheetID)

	// Итерируемся по ключам и значениям
	var newUrlFileTable string
	var newUrlFileDoc string

	for key, value := range document {
		fmt.Printf("Ключ: %s\n", key)

		switch key {
		case "Данные":

			data := value.(map[string]interface{})
			newUrlFileTable, err = p.scenarioTemplateTable(nameSheet, newSpreadsheetID, data)

			fmt.Printf("\nОбновленное Сведение: %+v\n", data)
			newUrlFileDoc, err = p.scenarioTemplateDoc(newDocumentID, newSpreadsheetID, data)

		default:
			fmt.Printf("Значение: %v\n", value)
		}

	}

	return newUrlFileTable, newUrlFileDoc, err
}
