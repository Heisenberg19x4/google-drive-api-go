package main

import (
	"encoding/json"
	"fmt"
	"path/filepath"
	"runtime"

	//"log"
	"testing"
)

var CLIENTINFO1 *ClientInfo

func TestAuth2(t *testing.T) {
	// Получаем путь к текущему файлу (main.go)
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("Error retrieving current file path.")
		return
	}

	// Получаем директорию, в которой находится main.go
	executableDir := filepath.Dir(filename)
	// Строим относительный путь к файлу credentials.json
	credentialsFile := filepath.Join(executableDir, "credentials", "credentials.json")

	fmt.Println("Credentials file path:\n", credentialsFile)

	clientInfo := &ClientInfo{credentialsFilePath: credentialsFile}

	clientInfo.getClientOAuth(credentialsFile)
	CLIENTINFO1 = clientInfo
}

func TestParse(t *testing.T) {
	// Распарсить JSON
	var document Document
	err = json.Unmarshal([]byte(jsonData), &document)
	if err != nil {
		fmt.Println("Ошибка при распарсивании JSON:", err)
		return
	}

	// Вывести результат
	fmt.Printf("%+v\n", document)

}
