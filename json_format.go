package main


type Data struct {
	Nomenclature []Nomenclature `json:"Номенклатура"`
	Number       int            `json:"Номер"`
	Date         string         `json:"Дата"`
	Counterparty Counterparty   `json:"Контрагент"`
	Images       Images         `json:"Картинки"`
}

type Nomenclature struct {
	Name             string `json:"Наименование"`
	Quantity         string `json:"Количество"`
	Number           string `json:"Номер"`
	UnitOfMeasurement string `json:"Единица измерения"`
	SizeWidth        string `json:"Размер.Ширина"`
}

type Counterparty struct {
	Name string `json:"Наименование"`
	INN  string `json:"ИНН"`
}

type Images struct {
	PhotoMine1 string `json:"ФотоМое1"`
	PhotoMine2 string `json:"ФотоМое2"`
}

type Information struct {
	StartingRow string     `json:"СтрНачальная"`
	Content     []Content  `json:"Cодержание"`
}

type Content struct {
	Title       string   `json:"Заголовок"`
	PageNumber  string   `json:"НомерСтраницы"`
	SubContent  []SubContent `json:"Cодержание"`
}

type SubContent struct {
	Title      string `json:"Заголовок"`
	PageNumber string `json:"НомерСтраницы"`
}

type Document struct {
	Information Information `json:"Сведения"`
	Data        Data        `json:"Данные"`
}

