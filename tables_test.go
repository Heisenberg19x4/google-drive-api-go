package main
 

import (
	"fmt"
	"path/filepath"
	"runtime"
    //"log"
	"testing"
)

var CLIENTINFO * ClientInfo

func TestAuth(t *testing.T) {
	// Получаем путь к текущему файлу (main.go)
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("Error retrieving current file path.")
		return
	}

	// Получаем директорию, в которой находится main.go
	executableDir := filepath.Dir(filename)
    // Строим относительный путь к файлу credentials.json
    credentialsFile := filepath.Join(executableDir, "credentials", "credentials.json")

    fmt.Println("Credentials file path:\n", credentialsFile)

	
	clientInfo := &ClientInfo{credentialsFilePath: credentialsFile}

	clientInfo.getClientOAuth(credentialsFile)
	
	clientInfo.credentialsFilePath=credentialsFile

	CLIENTINFO=clientInfo
}


func TestTableCreate(t *testing.T) {

	//TestAuth()
	// Create a new spreadsheet.
	tableWorker := &TableWorker{}
	tableWorker.clientInfo = CLIENTINFO
	tableWorker.clientDrive = &GoogleDriveWorker{CLIENTINFO}

	idTable, err := tableWorker.CreateTable("MyTable","")
	if err != nil {
		t.Errorf("Unable to create spreadsheet: %v", err)
	}
    t.Logf("Created spreadsheet ID: %v", idTable)
	
}


func TestTableCreateDir(t *testing.T) {
	tableWorker := &TableWorker{}
    tableWorker.clientInfo = CLIENTINFO
    tableWorker.clientDrive = &GoogleDriveWorker{CLIENTINFO}
    // Имя новой папки в Google Drive
    FolderName := "renjgkenrgjner"

    // Имя для новой таблицы
    tableName := "TestTable"

    // создаем папку
    folderID, err := tableWorker.clientDrive.createFolder(FolderName)
    if err != nil {
        t.Errorf("Error: %v", err)
    }

    // Создаем таблицу в указанной папке
    idTable, err1 := tableWorker.CreateTable(tableName, folderID)
    if err1 != nil {
        t.Errorf("Error: %v", err1)
    }

    t.Logf("Created spreadsheet ID: %v", idTable)
}
