FROM golang:1.21

RUN mkdir app
WORKDIR /app
COPY . .
EXPOSE 3334
# Download all the dependencies
RUN go get -d -v ./...
RUN go install -v ./...
RUN go build .
CMD ["./document_processor"]
