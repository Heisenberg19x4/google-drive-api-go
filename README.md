# Description

The project is created for personal use. I'm sharing it at this stage because I've encountered very few examples of working with the Google Drive API in Go. Here are some methods that might be useful for you:

- Authentication
- Working with spreadsheets (searching, reading, writing, comments...)
- Working with documents (searching, reading, writing, comments...)
- Working with PDFs
